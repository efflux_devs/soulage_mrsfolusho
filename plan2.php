<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Sickle Cell Walk | Soulage </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
    </head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
		.float_left {
			float: left;
			width: 10%;
		}
		.float_right {
			float: right;
			padding-left: 20px;
			width: 90%;
		}
		.fa_square {
			font-size: 40px;
		    color: #fff;
		    width: 90%;
		    background: #251021;;
		    height: 50%;
		    text-align: center;
		    border-radius: 30%;
		    margin-right: 20px;
		}
		.float_right_h1 {
			font-size: 18px;
			text-transform: capitalize;
			font-family: montserratReg;
			margin-bottom: 15px;
			font-weight: 600;
		}
	</style>
	<body>

		<?php
	        include ("header.php");
	    ?>

		<div class="w3-container">
	 
		</div>

		<div class="">
			<div class="div" style="background-color: #F2F2F2; padding: 30px;">
				<div class="container">
					<div class="col-md-12 w3layouts_register_right">
						<h1 style="text-align: center; font-size: 42px; font-family: montserratReg; text-transform: uppercase;">
							Sickle cell walk themed breaking the silence
						</h1>
						<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #FF0000; margin-bottom: 2em;"></p>

						<p style="color: #5F5F5F; font-size: 15px; line-height: 30px; margin-bottom: 20px; text-align: justify;">
							The sickle cell walk s a yearly activity which includes all Sickle cell NGOs prior to the world sickle cell day. This years activity took place n Victoria island  starting from Eko Atlantic and we were able to provide awareness, educate the public on how to manage sickle cell disorder patents, routine drugs to use, how to stop stigmatization of the patents, create awareness on having knowledge of ones genotype before marriage etc.
						</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<section class="banner-w3ls32">
			<div class="container">
			</div>
		</section>		
		

		<?php
	        include ("footer.php");
	    ?>

	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>
		<script>
			var myIndex = 0;
			carousel();

			function carousel() {
			    var i;
			    var x = document.getElementsByClassName("mySlides");
			    for (i = 0; i < x.length; i++) {
			       x[i].style.display = "none";  
			    }
			    myIndex++;
			    if (myIndex > x.length) {myIndex = 1}    
			    x[myIndex-1].style.display = "block";  
			    setTimeout(carousel, 3000); // Change image every 2 seconds
			}
		</script>
		<script>
	            var leftOffset = 0;
	            var moveHeading = function () {
	           
	            $("#heading").offset({ left: leftOffset });
	            leftOffset++;
	            if (leftOffset > 1200) {
	            leftOffset = 0;
	            }
	            };
	            setInterval(moveHeading, 30);
	        </script>
	<!-- //here ends scrolling icon -->
	</body>
</html>