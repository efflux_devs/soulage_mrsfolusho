<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Soulage | Free TCD TEST</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
        <style type="text/css">
            .dropdown-menu a:hover {background-color: #f1f1f1;}
            .dropdown:hover .dropdown-menu {
                display: block;
            }
            .dropdown-menu a {
                text-transform: uppercase;
            }
            .dropdown-menu {
                top: 95% !important;
            }
            .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
                background-color: red;
                width: 100%;
            }
            .top-menu ul li a {
                width: 100%;
            }
            .side_contact {
			width: 90%;
			height: auto;
			background-color: #251021;
			padding: 20px 40px;
			color: #fff;
		}

		.buttonn {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    color: #fff;
		    cursor: pointer;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		  }

		.buttonn:hover {
			color: red;
		}

		button.accordion {
		    background-color: #eee;
		    color: #251021;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}

		li {
			list-style: disc;
		}
        </style>
    </head>
	<body>

		<?php
	        include ("header.php");
	    ?>
		
		<div class="">
			<div class="div" style="background-color: #fff;">
				<div class="container">
					<div class="col-md-12 w3layouts_register_right">
						<p style="color: #777777; font-size: 15px; margin-bottom: 20px; padding-top: 30px; line-height: 30px; text-align: justify;">
							Transcranial Doppler test (TCD) is a screening of the brain to detect the risk of stroke. It has been noted that children with sickle cell disorder between the ages of 2 to 16 have high risk of having stroke and eventually get down with stroke if not adequately taken care of, hence the need to do the test before hand so as to prevent occurrence putting in mind that to prevent the stroke is less expensive and way better than the actual cure and treatment of it. <br><br>
							<b>Our aims and achievements:</b>
						</p>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div style="padding: 20px;">
								<button class="accordion">AIMS</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		<ul>
								  			<li>
								  				To prevent the increase in the percentage of the occurrence of child stoke in our beneficiaries
								  			</li>
								  			<li>
								  				To detect those with high risk and make sure the risk level reduces.
								  			</li>
								  			<li>
								  				To detect further occurrence of stroke in those that are already living with stroke.
								  			</li>
								  			<li>
								  				To make sure that medications to prevent and maintain stroke are available for the beneficiaries.
								  			</li>
								  		</ul>
								  	</p>
								</div>

								<button class="accordion">ACHIEVEMENTS</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		<ul>
								  			<li>
								  				40% of Soulage's beneficiaries between the ages of 2-16 were diagnosed with high risk while 5% of this age range currently live with stroke, with this information, Soulage foundation immediately commenced treatments
								  			</li>
								  			<li>
								  				 Free counselling were provided to the parents on how to manage and maintain the disease
								  			</li>
								  			<li>
								  				The test gave us insight on how to maintain and prevent possible future occurrence for our benefcares who are currently lvng wth stroke
								  			</li>
								  			<li>
								  				Free medications were provided to beneficiaries
								  			</li>
								  		</ul>
							  		</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div style="padding: 20px;">
								<div class="side_contact">
									<h2 style="margin-bottom: 15px; font-size: 20px; font-weight: 600; font-family: montserratReg; text-align: center;">How can we help you?</h2>
									<a href="contact.php" class="buttonn">Contact Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section class="banner-w3ls32">
			<div class="container">
			</div>
		</section>		
		

		<?php
	        include ("footer.php");
	    ?>

	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>

		<script>
			var myIndex = 0;
			carousel();

			function carousel() {
			    var i;
			    var x = document.getElementsByClassName("mySlides");
			    for (i = 0; i < x.length; i++) {
			       x[i].style.display = "none";  
			    }
			    myIndex++;
			    if (myIndex > x.length) {myIndex = 1}    
			    x[myIndex-1].style.display = "block";  
			    setTimeout(carousel, 3000); // Change image every 2 seconds
			}
		</script>

		<script>
            var leftOffset = 0;
            var moveHeading = function () {
           
            $("#heading").offset({ left: leftOffset });
            leftOffset++;
            if (leftOffset > 1200) {
            leftOffset = 0;
            }
            };
            setInterval(moveHeading, 30);
        </script>

        <script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			    acc[i].onclick = function(){
			        this.classList.toggle("active");
			        var panel = this.nextElementSibling;
			        if (panel.style.display === "block") {
			            panel.style.display = "none";
			        } else {
			            panel.style.display = "block";
			        }
			    }
			}
		</script>

		<script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    } 
			  }
			}
		</script>
	<!-- //here ends scrolling icon -->
	</body>
</html>