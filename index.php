<!DOCTYPE html>
<html>
	<head>
		<title>Soulage | Home</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="content/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
		<link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
		<script src="content/js/jquery-1.11.0.min.js"></script>
		<script src="app/lib/angular.min.js"></script>
		<script src="app/lib/angular-route.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="app/routes.js"></script>
		<!--start-smoth-scrolling-->
		<script type="text/javascript" src="content/js/move-top.js"></script>
		<script type="text/javascript" src="content/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="content/js/modernizr.custom.97074.js"></script>
		<script src="content/js/jquery.chocolat.js"></script>
			<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
			<!--light-box-files -->
			<script type="text/javascript" charset="utf-8">
			$(function() {
				$('.gallery-grids a').Chocolat();
			});
		</script>
		<style type="text/css">
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.dropdown-menu {
				top: 95% !important;
			}
			.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
				background-color: red;
				width: 100%;
			}
			.top-menu ul li a {
				width: 100%;
			}
		</style>
	</head>
	<body>
		<?php
            include ("header.php");
        ?>

		<!--welcome-starts-->
			<div class="welcome">
				<div class="container">
					<div class="welcome-top">
						<div class="col-md-4 welcome-left">
							<div class="welcomeimg"></div>
						</div>
						<div class="col-md-8 welcome-right">
							<h1 style="color: #FF000D; font-size: 36px; font-weight: 600; margin: 0px;">Welcome</h1>
							<p>
								Soulage Foundation is a non-governmental organization founded in Nigeria in 2011. Soulage has served indigent patients living with sickle cell disorder by supporting them with free medical services through an established Health Maintenance Organization. Late Mr. Olubiyi Odegbaike founded the organization to alleviate the ordeals of patients living with Sickle-cell disorder (SCD) and improve the quality of life of persons battling this genetic blood disorder.
							</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!--welcome-end-->

		<!--help-starts-->
			<div class="help">
				<h3 class="w3l_header two" style="color: #000; text-transform: uppercase; font-size: 60px; font-family: montserratReg; position: absolute; top: 3em; left: 4em;">I AM A SICKLE CELL WARRIOR</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #FF0000; position: absolute; top: 17em; left: 41em;"></p>
				<div class="help-top">
					<div class="help-left" style="background-color: #FF0000;">
						<h4>01</h4>
						<div class="tool">
							<a class="tooltips" href="#">
							<span></span></a>
						</div>
					</div>
					<div class="help-right">
						<h3 style="color: #FF0000;">Our Mission</h3>
						<p>
							Our mission is to reduce the pains of indigent living with sickle cell disorder by providing free medical support; provide enlightenment and awareness and to  help improve the quality of life of  indigent patients living with SCD.
						</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="help-btm">
				<img src="content/images/old_img_1.jpg" alt="" style="height: 15em; width: 30em;" />
			</div>
		<!--help-end-->

		<!--program-starts-->
			<div class="program">
				<h3 class="w3l_header two" style="color: #000; text-transform: uppercase; font-size: 60px; font-family: montserratReg; position: absolute; top: 3em; left: 1.5em;">BRINGING HOPE TO AFRICAN CHILD</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #FF0000; position: absolute; top: 17em; left: 41em;"></p>
				<div class="program-top">
					<div class="program-left">
						<h3>Our Vision</h3>
						<p>
							To support 5000 indigent patients living with Sickle Cell disorder and alleviate the sufferings of people with sickle cell 
							disorder in every way possible.
						</p>
					</div>
					<div class="program-right" style="background-color: #FF0000;">
						<h4>02</h4>
						<div class="tool1">
							<a class="tooltips1" href="#">
							<span></span></a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="program-btm">
				<img src="content/images/s1.jpg" alt="">
			</div>
		<!--program-end-->

		<!--testimonials-starts-->
			<div class="testimonials" style="background: #271C40;">
				<div class="container">
					<div class="testimonials-top heading">
						<h3 style="color: #FF0000;">Testimonials</h3>
					</div>
					<div class="testimonials-bottom">
						<div class="col-md-6 testimonials-left">
							<div class="test-left">
								<img src="" alt="" />
							</div>
							<div class="test-right">
								<p>I really appreciate the effort made by the <b>Soulage Foundation</b>for their support while I was in hospital.Till today they are still taking care of me.</p>
								<div class="tool2">
									<a class="tooltips2" href="#">
									<span></span></a>
								</div>
								<h4>Rashidat Oladipupo</h4>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-md-6 testimonials-left">
							<div class="test-left">
								<img src="" alt="" />
							</div>
							<div class="test-right">
								<p>We the entire family of Ojuade, wish to express our heartfelt to <b>Soulage foundation</b> for your support and assistance. </p>
								<div class="tool2">
									<a class="tooltips2" href="#">
									<span></span></a>
								</div>
								<h4>Ojuade Family</h4>
							</div>
							<div class="clearfix"></div>
						</div>
						<!-- <div class="col-md-4 testimonials-left">
							<div class="test-left">
								<img src="" alt="" />
							</div>
							<div class="test-right">
								<p>Curabitur consequat est quis semper consequat. Mauris non sem quis ipsum tempor porttitor vitae quis est. Mauris finibus eget nibh non vulputate.</p>
								<div class="tool2">
									<a class="tooltips2" href="#">
									<span></span></a>
								</div>
								<h4>Tomson</h4>
							</div>
							<div class="clearfix"></div>
						</div> -->
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!--testimonials-end-->

		<?php
            include ("footer.php");
        ?>
	</body>
</html>