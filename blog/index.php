<?php 
	require_once('../admin/process_query.php');
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Soulage | Blog</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,/>
		<script type="applijewelleryion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
		<!-- Custom Theme files -->
		<link rel="stylesheet" type="text/css" href="../content/css/fonts/font.css">
		<link href="css/style.css" rel='stylesheet' type='text/css' />	
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- animation-effect -->
		<link href="css/animate.min.css" rel="stylesheet"> 
		<script src="js/wow.min.js"></script>
		<script>
		 new WOW().init();
		</script>
		<!-- //animation-effect -->
	</head>
	
	<body>
		<div class="header" id="ban">
			<div class="container">
				<div class="header_right wow fadeInLeft animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
				<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav class="link-effect-7" id="link-effect-7">
							<ul class="nav navbar-nav">
								<li class="act"><a href="../index.php">Visit Main Website</a></li>
							</ul>
						</nav>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
				</div>
				<div class="clearfix"> </div>	
			</div>
		</div>
		<!--start-main-->
		<div class="header-bottom">
			<div class="container">
				<div class="logo wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
					<h1><a href="index.php">SOULAGE BLOG</a></h1>
					<!-- <p><label class="of"></label>LET'S MAKE A PERFECT STYLE<label class="on"></label></p> -->
				</div>
			</div>
		</div>
		<!-- banner -->

		<div class="services w3l wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
			<div class="container">
				<div class="grid_3 grid_5">
					<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">EVENT</a></li>
							<li role="presentation" class=""><a href="#safari" role="tab" id="safari-tab" data-toggle="tab" aria-controls="safari">FREE TCD TEST</a></li>
							<li role="presentation" class=""><a href="#trekking" role="tab" id="trekking-tab" data-toggle="tab" aria-controls="trekking">SICKLE CELL WALK</a></li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade" id="expeditions" aria-labelledby="expeditions-tab">
								
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/l2.jpg" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/l0.jpg" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/l1.jpg" class="img-responsive" alt="Wanderer">
								</div>
								<div class="clearfix"></div>
							</div>
							
							
							<div role="tabpanel" class="tab-pane fade" id="safari" aria-labelledby="safari-tab">
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/F1.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/f3.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/0076.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="clearfix"></div>
							</div>

							<div role="tabpanel" class="tab-pane fade active in" id="trekking" aria-labelledby="trekking-tab">

								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/ll4.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/k1.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="col-md-4 col-sm-5 tab-image">
									<img src="../content/images/k2.JPG" class="img-responsive" alt="Wanderer">
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- technology-left -->
		<div class="technology">
			<div class="container">
				<div class="col-md-9 technology-left">
					<div class="tech-no">
						<!-- technology-top -->
						<?php
						    while ($row=mysqli_fetch_array($query_result,MYSQLI_ASSOC)) {
						    $id = $row['id'];
						    $title = $row['post_title'];
						    $body = $row['post_body'];
						    $image = $row['post_image'];
						    $by = $row['post_by'];
						    $date = $row['signup_date'];
						?>
						<div class="wthree">
							<div class="col-md-6 wthree-left wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
								<div class="tch-img">
									<a href="more.php?id=<?php echo "$id"; ?>">
										<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 210px;">
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-6 wthree-right wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s" style="margin-bottom: 20px;">
								<h3>
									<a href="more.php?id=<?php echo "$id"; ?>">
										<?php echo "$title"; ?>
									</a>
								</h3>

								<h6 style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; width: 15em;">BY 
									<a href="more.php?id=<?php echo "$id"; ?>">
										<?php echo "$by"; ?> 
									</a>
									<?php echo "$date"; ?>.
								</h6>

								<p style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">
									<?php echo "$body"; ?>
								</p>
								
								<div class="bht1">
									<a href="more.php?id=<?php echo "$id"; ?>">Read More</a>
								</div>
								<div class="soci">
									<ul>
										<li class="hvr-rectangle-out">
											<a class="twit" href="#"></a>
											<a class="fb" href="#"></a>
											<!-- <iframe src="http://www.facebook.com/plugins/like.php?href=http://www.diamondsng.com/index.php?id=<?php echo "$id"; ?>&amp;layout=button_count&amp;show_faces=false&amp;width=260&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;height=21" scrolling="No" frameborder="0" style="border:none; overflow:hidden; width:260px; height:21px;" allowtransparency="true"></iframe> -->
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div> 
						</div>
						<?php }?>
					</div>
				</div>
				<!-- technology-right -->
				<div class="col-md-3 technology-right">	
					<div class="blo-top1">
						<div class="tech-btm">
							<h4>Popular Posts</h4>
							<?php
							    while ($row=mysqli_fetch_array($recent_query_result,MYSQLI_ASSOC)) {
							    $id = $row['id'];
							    $title = $row['post_title'];
							    $body = $row['post_body'];
							    $image = $row['post_image'];
							    $by = $row['post_by'];
							    $date = $row['signup_date'];
							?>
							<div class="blog-grids wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
								<div class="blog-grid-left">
									<a href="more.php?id=<?php echo "$id"; ?>">
										<div class="tch_img_div" style="background: url(../admin/uploads/<?php echo "$image"; ?>) no-repeat 0px 0px; background-size:cover; -webkit-background-size: cover; -o-background-size: cover; -ms-background-size: cover; -moz-background-size: cover; min-height: 100px;">
										</div>
									</a>
								</div>
								<div class="blog-grid-right">
									<h5>
										<a href="more.php?id=<?php echo "$id"; ?>" style="color: red;">
											<?php echo "$title"; ?>
										</a> 
									</h5>
									<p style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; ">
										<?php echo "$body"; ?>
									</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<?php }?>
						</div>
					</div>	
				</div>
				<div class="clearfix"></div>
				<!-- technology-right -->
			</div>
		</div>

		<div class="copyright wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
			<div class="container">
				<p>© 2016 soulage Blog. All rights reserved | Powered by <a href="http://effluxcompany.com/" target="_blank">Efflux</a></p>
			</div>
		</div>
	</body>
</html>