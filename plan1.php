<!DOCTYPE html>
<html lang="en">
	<head>
		<title>The Soulage March Event | Soulage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
	</head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}

		.side_contact {
			width: 90%;
			height: auto;
			background-color: #251021;
			padding: 20px 40px;
			color: #fff;
		}

		.buttonn {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    color: #fff;
		    cursor: pointer;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		  }

		.buttonn:hover {
			color: red;
		}

		button.accordion {
		    background-color: #eee;
		    color: #251021;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}

		li {
			list-style: disc;
		}
	</style>
	<body>
		<?php
	        include ("header.php");
	    ?>

		<div class="w3-container">
	 		
		</div>
		
		<div class="">
			<div class="div" style="background-color: #fff;">
				<div class="container">
					<div class="col-md-12 w3layouts_register_right">
						<p style="color: #777777; font-size: 15px; margin-bottom: 20px; padding-top: 30px; line-height: 30px; text-align: justify;">
							Introducing our beneficiaries to our sponsors Theme: Reducing Stigmatization through Enlightenment and Care an event which took place on the 11th of March 2017. <br><br>
							<b>Our aims and achievements:</b>
						</p>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div style="padding: 20px;">
								<button class="accordion">AIMS</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		<ul>
								  			<li>
								  				To introduce our beneficiaries to our partners.
								  			</li>
								  			<li>
								  				To provide a forum in which beneficiaries can express their feelings as well as their ordeal.
								  			</li>
								  			<li>
								  				To provide experts and counsellors to help tackle main issues and ordeals faced by the beneficiaries.
								  			</li>
								  			<li>
								  				To provide a sense of belonging for the beneficiaries.
								  			</li>
								  			<li>
								  				To provide a sense of hope for the beneficiaries.
								  			</li>
								  		</ul>
								  	</p>
								</div>

								<button class="accordion">ACHIEVEMENTS</button>
								<div class="panel">
								  	<p style="font-size: 15px; color: #777777; text-align: justify;">
								  		<ul>
								  			<li>
								  				Through the partnership with HYGEIA HMO, we were able to provide counsellors to help give medical advice to beneficiaries.
								  			</li>
								  			<li>
								  				The theme of the event was addressed by Dr. Annette Akinsete (CEO Sickle Cell Foundation Of Nigeria) which helped give a better understanding of SCD.
								  			</li>
								  			<li>
								  				Beneficiaries were able to express themselves and narrate their ordeal.
								  			</li>
								  			<li>
								  				Two elderly living survivors were provided  and they were also able to give advice as to how to live a better life with or without SCD.
								  			</li>
								  			<li>
								  				Media coverage
								  			</li>
								  			<li>
								  				The attendance of the Ministry of Health 
								  			</li>
								  		</ul>
							  		</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div style="padding: 20px;">
								<div class="side_contact">
									<h2 style="margin-bottom: 15px; font-size: 20px; font-weight: 600; font-family: montserratReg; text-align: center;">How can we help you?</h2>
									<a href="contact.php" class="buttonn">Contact Us</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section class="banner-w3ls32">
			<div class="container">
			</div>
		</section>		
		

		<?php
	        include ("footer.php");
	    ?>

	<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
		<script type="text/javascript">
			$(document).ready(function() {
				/*
					var defaults = {
					containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
					};
				*/
									
				$().UItoTop({ easingType: 'easeOutQuart' });
									
				});
		</script>

		<script>
			var myIndex = 0;
			carousel();

			function carousel() {
			    var i;
			    var x = document.getElementsByClassName("mySlides");
			    for (i = 0; i < x.length; i++) {
			       x[i].style.display = "none";  
			    }
			    myIndex++;
			    if (myIndex > x.length) {myIndex = 1}    
			    x[myIndex-1].style.display = "block";  
			    setTimeout(carousel, 3000); // Change image every 2 seconds
			}
		</script>

		<script>
            var leftOffset = 0;
            var moveHeading = function () {
           
            $("#heading").offset({ left: leftOffset });
            leftOffset++;
            if (leftOffset > 1200) {
            leftOffset = 0;
            }
            };
            setInterval(moveHeading, 30);
        </script>

        <script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			    acc[i].onclick = function(){
			        this.classList.toggle("active");
			        var panel = this.nextElementSibling;
			        if (panel.style.display === "block") {
			            panel.style.display = "none";
			        } else {
			            panel.style.display = "block";
			        }
			    }
			}
		</script>

		<script>
			var acc = document.getElementsByClassName("accordion");
			var i;

			for (i = 0; i < acc.length; i++) {
			  acc[i].onclick = function() {
			    this.classList.toggle("active");
			    var panel = this.nextElementSibling;
			    if (panel.style.maxHeight){
			      panel.style.maxHeight = null;
			    } else {
			      panel.style.maxHeight = panel.scrollHeight + "px";
			    } 
			  }
			}
		</script>
	<!-- //here ends scrolling icon -->
	</body>
</html>