<?php
  require_once('connect.php');
  
  if (isset($_POST) & !empty($_POST)) {
    $username = mysqli_real_escape_string($connection, $_POST['username']);
    $email = mysqli_real_escape_string($connection, $_POST['email']);
    $password = $_POST['password'];
    $salt      = 'hello_1m_@_SaLT';
    $hashed    = hash('sha256', $password . $salt);

    $sql = "INSERT INTO register (username, email, password, salt) VALUES ('$username', '$email', '$hashed', '$salt')";
    $result = mysqli_query($connection, $sql);

    if ($result) {
      $smsg = "User Registration Successfull";
    }else{
      $fmsg = "User Registration Failed";
    }
  }
?>

<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.min.css'>
</head>
<body>
  <?php if(isset($smsg)){?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
  <?php if(isset($fmsg)){?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
</body>
</html>