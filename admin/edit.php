<?php
	session_start();
	require_once('connect.php');

	if(!isset($_SESSION['username'])) {
		header("Location: login.php");
		die();
	}

	$id = $_GET['id'];

    $id_get = "SELECT * FROM blogpost WHERE id='$id'";
	$id_result = mysqli_query($connection, $id_get);
	
	$row_id=mysqli_fetch_assoc($id_result);
    $id = $row_id['id'];
	$title = $row_id['post_title'];
    $body = $row_id['post_body'];
    $image = $row_id['post_image'];
    $by = $row_id['post_by'];
    $date = $row_id['signup_date'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>New Post</title>
	<link rel="stylesheet" type="text/css" href="../content/css/bootstrap.css">
	<script src="../content/js/jquery-1.11.0.min.js"></script>
	<script src="../content/js/bootstrap.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="new.php">Soulage</a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="new.php">Create New</a></li>
		        <li><a href="posts.php">Posts</a></li>
		      </ul>
		      <ul class="nav navbar-nav navbar-right">
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="logout.php">Logout</a></li>
		          </ul>
		        </li>
		      </ul>
		    </div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container" style="background-color: #ddd;">
	  <h2>Edit Blog Post</h2>
	  <form class="form-horizontal" action="process_edit.php" method="POST" enctype="multipart/form-data">
	    <div class="form-group">
	      <label class="control-label col-sm-2">Post Title:</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" id="post_title" placeholder="" value="<?php echo "$title"; ?>" name="post_title">
	      </div>
	    </div>
	    
	    <div class="form-group">
	      <label class="control-label col-sm-2">Post Body:</label>
	      <div class="col-sm-10">  
	      	<textarea type="text" class="form-control" placeholder="" id="post_body" name="post_body" style="width: 100%; max-width: 100%; height: 250px; max-height: 250px;"><?php echo "$body"; ?></textarea>        
	      </div>
	    </div>

	    <div class="form-group">
	      <label class="control-label col-sm-2">Choose File:</label>
	      <div class="col-sm-10">
	        <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
	      </div>
	    </div>

	    <div class="form-group">
	      <label class="control-label col-sm-2">Post By:</label>
	      <div class="col-sm-10">
	        <input type="text" class="form-control" id="post_by" placeholder="" value="<?php echo "$by"; ?>" name="post_by">
	      </div>
	    </div>
	    <input type="hidden" name="id" id="id" value="<?php echo "$id"; ?>">

	    <div class="form-group">        
	      <div class="col-sm-offset-2 col-sm-10">
	        <button type="submit" class="btn btn-default" name="submit">Submit</button>
	      </div>
	    </div>
	  </form>
	</div>
</body>
</html>