<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Soulage | Home</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="content/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
		<link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
		<script src="content/js/jquery-1.11.0.min.js"></script>
		<script src="app/lib/angular.min.js"></script>
		<script src="app/lib/angular-route.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="app/routes.js"></script>
		<!--start-smoth-scrolling-->
		<script type="text/javascript" src="content/js/move-top.js"></script>
		<script type="text/javascript" src="content/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="content/js/modernizr.custom.97074.js"></script>
		<script src="content/js/jquery.chocolat.js"></script>
			<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
			<!--light-box-files -->
			<script type="text/javascript" charset="utf-8">
			$(function() {
				$('.gallery-grids a').Chocolat();
			});
		</script>
		<style type="text/css">
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.dropdown-menu {
				top: 95% !important;
			}
			.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
				background-color: red;
				width: 100%;
			}
			.top-menu ul li a {
				width: 100%;
			}
			.teamimg_1 {
			height: 160px;
			background-image: url(content/images/biyi.jpg);
			background-size: 100% auto;
			background-repeat: no-repeat;
			border:4px solid black;
		}
		.teamimg_3 {
			height: 160px;
			background-image: url(content/images/ike_adebowale.jpg);
			background-size: auto 100%;
			background-repeat: no-repeat;
			border:4px solid black;
		}
		</style>
	</head>
<body>
	
	<?php
        include ("header.php");
    ?> 	

    <div class="container_background">
    	<div class="container">
	    	<div class="row">
	    		<h1 style="text-align: center; font-weight: 600; margin: 20px 0px; font-size: 50px; color: #fff;">
	    			OUR TEAM
	    		</h1>
			    <div class="col-md-2 col-md-offset-1">
			    	<a href="#section1">
			    		<div class="teamimg_1"></div>
			    		<h3 style="color: #ffffff; text-align: center; margin-top: 10px;">Late Biyi Odegbaike</h3>
			    	</a>
			    </div>
			    <div class="col-md-2">
			    	<a href="#section2">
			    		<img src="content/images/folusho_ode.jpg" class="img-responsive" style="width: 100%; height: 160px; border:4px solid black;">
			    		<h3 style="color: #ffffff; text-align: center;  margin-top: 10px;">Folusho Odegbaike
			    		</h3>
			    	</a>
			    </div>
			    <div class="col-md-2">
			    	<a href="#section3">
			    		<div class="teamimg_3"></div>
			    		<h3 style="color: #ffffff; text-align: center; margin-top: 10px;">Ikeoluwapo Adebowale
			    		</h3>
			    	</a>
			    </div>
			    <div class="col-md-2">
			    	<a href="#section4">
			    		<img src="content/images/shola_ayeni.jpg" class="pic4" style="width: 100%; height: 160px; border:4px solid black;">
			    		<h3 style="color: #ffffff; text-align: center; margin-top: 10px;">Olushola Ayeni
			    		</h3>
			    	</a>
			    </div>
			    <div class="col-md-2">
			    	<a href="#section5">
			    		<img src="content/images/seun_ayinla.jpg" class="pic5" style="width: 100%; height: 160px;border:4px solid black;">
			    		<h3 style="color: #ffffff; text-align: center; margin-top: 10px;">Oluwaseun Ayinla
			    		</h3>
			    	</a>
			    </div>
			</div>
	    </div>
    </div>

    <div class="container">
    	<p style="height: 100px;"></p>
    	<div id="section1" style="margin-bottom: 50px;"></div>
    	<div class="scroll_1">
    		<h1 style="font-family: lato !important;">LATE BIYI ODEGBAIKE</h1>
    		<P style="font-family: lato !important;">

				Mr Biyi Odegbaike is the founder of Soulage Foundation. He has a lofty dream to help the indigent children who are suffering from sickle cell disease to get free medical care and give hope to the worried parent that their wards will live long like every normal child. Mr Biyi had always had a soft spot in his heart for the children and had always been involved in humanitarian services to help the children of the world. However, after he witnessed the agony of the parent of a dying a child in one of his visits to he hospital, he decided to start a foundation to cater for this category of people. <br><br>
    		</P>
    	</div>

    	<div class="scroll_2" id="section2">
    		<h1 style="font-family: lato !important;">FOLUSHO ODEGBAIKE</h1>
    		<p style="font-family: lato !important;">
    			Folusho Odegbaike, the lead consultant at HYT Consulting is an experienced Organizational Development Professional with expertise in People Development, Process Re-engineering and Resources Optimization. She is a certified SAP HCM consultant and an accomplished human capital management consultant with over 15 years work experience in Human Capital Learning and Development, Performance Management, Customer Service and other core Human Resources and Administration functions, which has given her a clear edge as a Human Capital Management Professional.<br><br>

				Folusho took over the administration of Soulage after the demise of Biyi Odegbaike, the drive to continue Soulage foundation came through from emotions and her experience with her late husband Biyi Odegbaike who had firsthand experience of the crisis. Having given the opportunity to meet some of the beneficiaries during the 2016 open house and listen to their experiences, Folusho took it as a mission to not just continue soulage but also to get more beneficiaries and touch more lives. 
    		</p>
    	</div>

    	<div class="scroll_3" id="section3">
    		<h1 style="font-family: lato !important;">IKEOLUWAPO ADEBOWALE</h1>
    		<p style="font-family: lato !important;">
    			Ikeoluwapo Adebowale is an HR Professional with over 15 years work experience of which 11 years is in Human Resources practice with exposure to Best Practice in various industries. 
    			<br><br>
				Ikeoluwapo has functioned within the backend areas of Human Resources and these areas include HR Information Systems, performance management, Compensation & Benefits, Personnel Administration, Resourcing and Payroll where her achievements has spanned redesigning processes, development and implementation of policies and organizational restructuring among other things. 
				<br><br>
				Her journey with soulge foundation started from her love for children and since soulage foundation deals more with children, she started giving her all to the success of the foundation and the wellbeing of the beneficiaries. She sees soulage foundation as not just a foundation but also as a home for the beneficiaries to get comfort, to continue to live their lives without the fear of not having the basic health care.
    		</p>
    	</div>

    	<div class="scroll_4" id="section4">
    		<h1 style="font-family: lato !important;">OLUSHOLA AYENI</h1>
    		<p style="font-family: lato !important;">
    			She is the Chief Operating Officer in Cloud interactive Limited and coordinates the operations in the West African region in the company. She holds a B.Sc Honors Degree in Education and Economics and an Associate Membership at the Nigerian Institute of Brand Management. 
    			<br><br>
				She sees soulage as life with one major attributes of it being that when you don’t feed it, nurture it, take care of it, it dies, and with soulage, many lives are involved which she has decided to continue to support so that these lives can be saved.
    		</p>
    	</div>

    	<div class="scroll_5" id="section5">
    		<h1 style="font-family: lato !important;">OLUWASEUN O. AYINLA</h1>
    		<p style="font-family: lato !important;">
    			Seun Ayinla is a professional with over 13 years work experience in strategy, brand management, mergers and acquisition. She is an operation expert and internal audit with track record of delivering results focused on business acquisition, quality assurance, productivity, and consumer driven bottom-line growth. 
    			<br><br>
				Seun Ayinla sees the importance of health care for sickle cell patients, she sees the need to continue to give hope and create awareness about sickle cell, she joined soulage foundation team with the main aim of making sure that the beneficiaries not just get the care they need but also the drive to move and forge ahead in their various endeavors.
    		</p>
    	</div>
    </div>

	<?php
        include ("footer.php");
    ?> 

    <script type="">
    	$(document).ready(function(){
		  $("a").on('click', function(event) {

		    if (this.hash !== "") {
		      event.preventDefault();

		      var hash = this.hash;

		      $('html, body').animate({
		        scrollTop: $(hash).offset().top
		      }, 800, function(){
		   
		        window.location.hash = hash;
		      });
		    }
		  });
		});
    </script>

    <script src="js/jquery.vide.min.js"></script>
<!-- js-scripts -->					
		
			<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
			
		<!-- Baneer-js -->
		<script src="js/responsiveslides.min.js"></script>
		<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						pager:false,
						nav: true,
						speed: 1000,
						namespace: "callbacks",
						before: function () {
							$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
						}
					});
				});
			</script>
		<!-- //Baneer-js -->
		
		<!-- For-Gallery-js -->
			<!-- script for portfolio -->
			<script type='text/javascript' src='js/jquery.easy-gallery.js' ></script>
			<script type='text/javascript'>
			  //init Gallery
			  $('.portfolio').easyGallery();
			</script>
			<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$('#horizontalTab').easyResponsiveTabs({
						type: 'default', //Types: default, vertical, accordion           
						width: 'auto', //auto or any width like 600px
						fit: true   // 100% fit in a container
					});
				});		
			</script>
			<!-- //script for portfolio -->
		<!-- //For-Gallery-js -->

		<!-- start-smoth-scrolling -->
				<script type="text/javascript" src="js/move-top.js"></script>
				<script type="text/javascript" src="js/easing.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
				</script>
		<!-- start-smoth-scrolling -->

<!-- //js-scripts -->
</body>
</html>