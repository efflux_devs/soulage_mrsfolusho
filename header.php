<?php
function active($currect_page){
  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if($currect_page == $url){
      echo 'active'; //class name in css 
  } 
}
?>
<!--header-starts-->
	<div class="header" id="home">
		<div class="container">
			<div class="header-main">
				<div class="col-md-6 header-left">
					<a href="index.php">
						<img src="content/images/logo_1.png" style="height: 80px; width: 50%;">
					</a>
				</div>
				<div class="col-md-6 header-right">
					<ul>
						<li>
							<a href="#">
								<i class="fa fa-facebook-square" aria-hidden="true" style="font-size: 35px; color: #FF000D;"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-twitter-square" aria-hidden="true" style="font-size: 35px; color: #FF000D;"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-linkedin-square"  aria-hidden="true" style="font-size: 35px; color: #FF000D;"></i></a>
							</a>
						</li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
<!--header-ends--> 

<!--banner-starts-->
	<div class="bnr">
		<div  id="top" class="callbacks_container">
			<ul class="rslides" id="slider4" style="width: 100%;height: 600px !important;">
			    <li>
					<div class="banner1">	
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 170px; width: 100%; bottom: 250px; color: #fff; font-size: 80px; text-transform: uppercase;"><span>BE A HERO</span> SAVE A LIFE</h3>					
					</div>
				</li>
				<li>
					<div class="banner2">
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 320px; width: 100%; bottom: 300px; color: #fff; font-size: 80px; text-transform: uppercase;">FREE TCD TEST</h3>						
					</div>
				</li>
				<li>
					<div class="banner3">
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 400px; width: 100%; bottom: 300px; color: #fff; font-size: 80px; text-transform: uppercase;">MEDICATION</h3>					
					</div>
				</li>
				<!-- <li>
					<div class="banner4">
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 250px; width: 100%; bottom: 300px; color: #fff; font-size: 80px; text-transform: uppercase;">SICKLE CELL WALK</h3>							
					</div>
				</li> -->
				<li>
					<div class="banner5">
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 40px; width: 100%; bottom: 250px; color: #fff; font-size: 80px; text-transform: uppercase;">LET'S BREAK THE SICKLE CELL CYCLE</h3>		
					</div>
				</li>
				<li>
					<div class="banner6">
						<h3 class="animated fadeInLeft " class="clat" style="font-family: montserratReg; position: absolute; right: 0px; left: 40px; width: 100%; bottom: 250px; color: #fff; font-size: 80px; text-transform: uppercase;">TRANSFORM A CHILD'S LIFE</h3>	
					</div>
				</li>
			</ul>
		</div>
		<div class="clearfix"> </div>
	</div>
<!--banner-ends--> 

<!--Slider-Starts-Here-->
	<script src="content/js/responsiveslides.min.js"></script>
	<script>
	    // You can also use "$(window).load(function() {"
	    $(function () {
	      // Slideshow 4
	      $("#slider4").responsiveSlides({
	        auto: true,
	        pager: true,
	        nav: true,
	        speed: 500,
	        namespace: "callbacks",
	        before: function () {
	          $('.events').append("<li>before event fired.</li>");
	        },
	        after: function () {
	          $('.events').append("<li>after event fired.</li>");
	        }
	      });
	
	    });
	</script>
<!--End-slider-script-->

<!--header-starts-->
	<div class="header-bottom">
		<div class="fixed-header">
			<div class="container">
				<div class="top-menu">
					<span class="menu"><img src="content/images/menu-icon.png" alt="" /></span>
					<ul class="nav">
						<li>
							<a class="hvr-bounce-to-right <?php active('index.php');?>" href="index.php">
								Home
							</a>
						</li>

						<li class="dropdown">
							<a href="about.php" class="hvr-bounce-to-right <?php active('about.php');?>">
								About
							</a>
							<ul class="dropdown-menu agile_short_dropdown">
			                  <li style="list-style: none; width: 100%;"><a href="whoweare.php">Who we are</a></li>
			                  <li style="list-style: none; width: 100%"><a href="vision.php">Vision and Mission</a></li>
			                  <li style="list-style: none; width: 100%"><a href="ourteam.php">Our Team</a></li>
			                </ul>
						</li>
						<li class="dropdown">
							<a href="plan.php" class="hvr-bounce-to-right <?php active('plan.php');?>">Our Plan</a>
							<ul class="dropdown-menu agile_short_dropdown">
			                    <li style="list-style: none; width: 100%;"><a href="plan.php">Free TCD Test</a></li>
			                    <li style="list-style: none; width: 100%;"><a href="plan1.php">The Soulage March Event</a></li>
			                    <li style="list-style: none; width: 100%;"><a href="plan2.php">Sickle Cell Walk</a></li>
			                    <li style="list-style: none; width: 100%;"><a href="plan3.php">Free Genotype Testing</a></li>
			                    <li style="list-style: none; width: 100%;"><a href="plan4.php">Blood Drive</a></li>
			                </ul>
						</li>
						<li><a href="sponsor.php" class="hvr-bounce-to-right <?php active('sponsor.php');?>">Why Sponsor?</a></li>
						<li><a href="gallery.php" class="hvr-bounce-to-right <?php active('gallery.php');?>">Portfolio</a></li>
						<li><a href="donate.php" class="hvr-bounce-to-right <?php active('donate.php');?>">Donate</a></li>
						<li><a href="blog/index.php" class="hvr-bounce-to-right <?php active('blog.php');?>">Blog</a></li>
						<li><a href="contact.php" class="hvr-bounce-to-right <?php active('contact.php');?>">Contact</a></li>
					</ul>	
					<!-- script for menu -->
						<script>
							$( "span.menu" ).click(function() {
							  $( "ul.nav" ).slideToggle( "slow", function() {
								// Animation complete.
							  });
							});
						</script>
					<!-- script for menu -->
				</div>
				<script>
					$(document).ready(function() {
						var navoffeset=$(".header-bottom").offset().top;
						$(window).scroll(function(){
							var scrollpos=$(window).scrollTop(); 
							if(scrollpos >=navoffeset){
								$(".header-bottom").addClass("fixed");
							}else{
								$(".header-bottom").removeClass("fixed");
							}
						});
					});
				</script>
			</div>
		</div>
	</div>
<!--header-ends -->