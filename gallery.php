<head>
	<title>Portfolio | Soulage</title>
     <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
         <link rel="stylesheet" type="text/css" media="all" href="c/css/animate.css">
         <link rel="shortcut icon" type="image/png" href="favicon.png"/>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link href="content/css/index.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="js/jquery.js"></script>
        <script src="css/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $( function() {
                $( "#tabs" ).tabs();
            } );
        </script>

        <style>
            .dropdown-menu a:hover {background-color: #f1f1f1;}
            .dropdown:hover .dropdown-menu {
                display: block;
            }
            .dropdown-menu a {
                text-transform: uppercase;
            }
            .main_body {
                margin: 50px 0px !important;
            }
            .img_133 {
                background-image: url("blooddrive/1.JPG");
                height: 200px;
                background-size: auto 100%;
                background-repeat: no-repeat;
            }
        </style>
</head>
<body style="background: #fff;">

    <?php
        include ("header.php");
    ?>

    <div class="container project_css">
        <div class="row">
            <div class="col-md-12">
                <div class="main_body">
                    <table style="margin-bottom: 30px;">
                        <tbody>
                            <tr>
                                <th style="text-transform: uppercase; color: #251021; font-family: montserratReg;">Our Portfolio</th>
                            </tr>
                        </tbody>
                    </table>

                    <div class="row" style="margin-bottom: 30px;">
                        <div class="col-md-3 outside_border">
                            <div class="thick_border">
                                <div class="img_9 portfolio_1">
                                    
                                </div>
                            </div>
                            <a href="freetcdtest.php">
                                <div class="on_hover_effect ">
                                    <table>
                                        <tr>
                                            <th class="animated rollin" style="font-family: montserratReg;">
                                                Free TCD Test
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-3 outside_border">
                            <div class="thick_border">
                                <div class="img_10 portfolio_1"></div>
                            </div>
                            <a href="sicklecellwalk.php">
                                <div class="on_hover_effect ">
                                    <table>
                                        <tr>
                                            <th class="animated rollin" style="font-family: montserratReg;">
                                                Sickle Cell Walk
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                        
                        <div class="col-md-3 outside_border">
                            <div class="thick_border">
                                <div class="img_11 portfolio_1"></div>
                            </div>
                            <a href="freegenotypetest.php">
                                <div class="on_hover_effect ">
                                    <table>
                                        <tr>
                                            <th class="animated rollin" style="font-family: montserratReg;">
                                                Free Genotype Test
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-3 outside_border">
                            <div class="thick_border">
                                <div class="img_12 portfolio_1"></div>
                            </div>
                            <a href="soulagevent.php">
                                <div class="on_hover_effect ">
                                    <table>
                                        <tr>
                                            <th class="animated rollin" style="font-family: montserratReg;">
                                                Soulage Event
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 outside_border">
                            <div class="thick_border">
                                <div class="img_133 portfolio_1"></div>
                            </div>
                            <a href="blooddrive.php">
                                <div class="on_hover_effect ">
                                    <table>
                                        <tr>
                                            <th class="animated rollin" style="font-family: montserratReg;">
                                                Blood Drive
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        include ("footer.php");
    ?>
</body>