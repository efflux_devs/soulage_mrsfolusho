<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Soulage | Sponsor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
    </head>
	<style>
		.mySlides {display:none;height: 600px;}
		.dropdown-menu a:hover {background-color: #f1f1f1;}
		.dropdown:hover .dropdown-menu {
		    display: block;
		}
		.dropdown-menu a {
			text-transform: uppercase;
		}
		.side_contact {
			width: 90%;
			height: auto;
			background-color: #251021;
			padding: 20px 40px;
		}

		.button {
			text-align: center;
			width: 80%;
		    margin: auto;
		    display: block;
		    text-align: center;
		    color: #fff;
		    cursor: pointer;
		    font-weight: 600;
		    font-family: montserratReg;
		    font-size: 20px;
		}
		.button:hover {
			color: #FF000D;
		}

		button.accordion {
		    background-color: #eee;
		    color: #251021;
		    cursor: pointer;
		    padding: 18px;
		    font-family: montserratReg;
		    width: 100%;
		    border: none;
		    text-align: left;
		    outline: none;
		    font-size: 15px;
		    text-transform: uppercase;
		    transition: 0.4s;
		}

		button.accordion.active, button.accordion:hover {
		    background-color: #ddd;
		}

		button.accordion:after {
		    content: '\002B';
		    color: #777;
		    font-weight: bold;
		    float: right;
		    margin-left: 5px;
		}

		button.accordion.active:after {
		    content: "\2212";
		}

		div.panel {
		    padding: 0 18px;
		    background-color: #F2F2F2;
		    max-height: 0;
		    line-height: 30px;
		    overflow: hidden;
		    transition: max-height 0.2s ease-out;
		}

		.carousel-caption {
		    left: 12%;
		    padding-bottom: 5%;
		    right: 0;
		    top: 25%;
		    text-align: left;
		}

		.caption_1 {
		    left: 3%;
		    padding-bottom: 5%;
		    right: 0;
		    top: 35%;
		    text-align: left;
		}
	</style>
<body>

	<?php
        include ("header.php");
    ?>

	<div class="w3-container">
 
	</div>

	<div class="banner-bottom-icons">
		<div class="">
			<div class="col-md-12 w3_banner_bottom_icons_right" style="padding: 0px 0px 20px 0px; background-color: #fff; font-family: opensans;">
				<div class="container">
					<h1 style="text-align: center; font-size: 36px; margin: 25px 0px; font-weight: 600; color: #251021; font-family: montserratReg;">
						BECOME A SOULAGE PARTNER
					</h1>
					
					<p style="color: #777777; padding: 20px 20px; font-size: 15px; line-height: 30px; text-align: justify;">
						The Soulage foundation is a long-running national organization advocating and serving the needs of the sickle cell community. The foundation is grateful for the support of the corporations and businesses across the country that contribute to our mission of improving the quality of health, life and services for individuals and families affected by sickle cell disease and related conditions, while promoting the serach for a cure for all people in the world.<br><br>

						<b>CSR PARTNER:</b>
						Corporate organizations and Multi-nationals could partner with soulage foundation for sickle cell as part of your organisations corporate social responibly. A quaterly report of how runs and the expenses will be sent for follow up and documentation purposes. <br><br>

						<b>STATEGIC PARTNER:</b>
						Health management organizationshospitals and private health consulting firms that shows interest iin our projects and wish to support by providing us with health products and services. <br> <br>

						<b>INDIVIDUAL PARTNER:</b>
						This could come in form individual sponsorship or a group of individuals for example the staffs of a particular company or members of a particular organisation.For this category too, reports will be sent to the representatives and concerened individuals.
					</p>

					<p style="font-size: 15px; padding: 20px 20px 20px 10px; background-color: #251021; margin-left: 20px; border-left: 10px solid #FF000D; color: #fff;">
						<em>
							NB: ANONYMOUS PARTNERS ARE ALSO ALLOWED.
						</em>
					</p>

					<!-- <h1 style="text-align: center; font-size: 36px; margin: 25px 0px 0px 0px; font-weight: 600; color: #251021; font-family: montserratReg; text-transform: uppercase;">
						sponsorship categories:
					</h1> -->
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- <div class="whitebackgroundbody">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div style="padding: 20px;">
							<button class="accordion">CORPORATE SPONSORS</button>
							<div class="panel">
							  	<ul>
								  	<li style="list-style: disc;">
								  		Recognition on all promotional material
								  	</li>

								  	<li style="list-style: disc;">
								  		Recognition on website including a link to corporate sponsors website
								  	</li>

								  	<li style="list-style: disc;">
								  		Right to display banners around event venue
								  	</li>

								  	<li style="list-style: disc;">
								  		5 complimentary tickets to the event
								  	</li>
							  	</ul>
							</div>

							<button class="accordion">SPONSORSHIP IN KIND</button>
							<div class="panel">
							  	<ul>
								  	<li style="list-style: disc;">
								  		Right to be named official company
								  	</li>

								  	<li style="list-style: disc;">
								  		Right to name on print advertisement
								  	</li>

								  	<li style="list-style: disc;">
								  		Sponsor name/logo in promotional brochure
								  	</li>

								  	<li style="list-style: disc;">
								  		Sponsor name/logo on event t-shirt and goodie bags
								  	</li>
							  	</ul>
							</div>

							<button class="accordion">SUPPORTING SPONSORS</button>
							<div class="panel">
							  	<ul>
								  	<li style="list-style: disc;">
								  		Recognition on the Soulage foundation website
								  	</li>

								  	<li style="list-style: disc;">
								  		Presence on event brochure
								  	</li>

								  	<li style="list-style: disc;">
								  		Media exposure from our media partners
								  	</li>

								  	<li style="list-style: disc;">
								  		Recognition during event for contributing to the sickle cell community
								  	</li>
							  	</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div style="padding: 20px;">
							<div class="side_contact">
								<h2 style="margin-bottom: 15px; text-align: center; font-size: 20px; font-weight: 600; font-family: montserratReg; color: #fff;">How can we help you?</h2>
								<a href="contact.php" class="button">Contact Us</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	</div>

	<div class="clearfix"></div>

	<div class="clearfix"></div>

	<?php
        include ("footer.php");
    ?>

<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>
	<script>
        var leftOffset = 0;
        var moveHeading = function () {
       
        $("#heading").offset({ left: leftOffset });
        leftOffset++;
        if (leftOffset > 1200) {
        leftOffset = 0;
        }
        };
        setInterval(moveHeading, 30);
    </script>
<!-- //here ends scrolling icon -->

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        var panel = this.nextElementSibling;
		        if (panel.style.display === "block") {
		            panel.style.display = "none";
		        } else {
		            panel.style.display = "block";
		        }
		    }
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
		  }
		}
	</script>

	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>
</body>
</html>