<!DOCTYPE html>
<html>
    <head>
        <title>Contact Us | Soulage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="c/css/contact.css">
        <link rel="stylesheet" type="text/css" href="c/css/style.css"> -->
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
    </head>
    <body>

        <?php
            include ("header.php");
        ?>
        
        <div class="banner_wrap">
            <div class="baner"></div>
        </div>

        <table style="width: 100%;">
            <tr>
                <th style="text-align: center; padding: 10px 0px; font-size: 40px; font-family: montserratReg; text-transform: capitalize; color: #251021;">How can we help you?</th>
            </tr>
            <tr>
                <td style="text-align: center; padding: 10px 0px; font-size: 20px; font-family: opensans; text-transform: capitalize;">
                    Drop us a line...We're glad to be of service.
                </td>
            </tr>
        </table>

        <div class="container" style="margin-bottom: 30px;">
            <div class="row">
                <div class="col-md-8">
                    <form name="registration_form" id="registration_form" class="form-horizontal" action="process_contact_form.php" method="POST">
                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Full Name:</label>
                                <input type="text" class="form-control input" name="fullname" id="fullname" placeholder="Full Name" required>
                           </div>
                        </div>

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Email Address:</label>
                                <input type="email" class="form-control input" name="emailaddress" id="emailaddress" placeholder="Enter Email Address" required>
                           </div>
                        </div><!--/form-group-->

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Subject:</label>
                                <input type="text" class="form-control input" name="subject" id="subject" placeholder="Enter Subject" required>
                           </div>
                        </div><!--/form-group-->

                        <div class="form-group form_wrap">      
                           <div class="col-sm-12">
                                <label for="sur" class="lab">Message:</label> <br>
                                <textarea class="textarea" name="message" rows="5"></textarea>
                           </div>
                        </div><!--/form-group-->
                        <input type="submit" value="Send Message" class="button">
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="right_wrap contact_1">
                        <div class="">
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        ENQUIRIES:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        For General Enquiries
                                    </td>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-envelope" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        info@soulagefoundation.org
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="">
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        REGISTERED OFFICE ADDRESS:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-map-marker" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        13A, Fagba Crescent,
                                    </td>
                                </tr>
                                <tr >
                                    <td class="td">
                                        Off Lateef Jakande Rd,
                                    </td> <br>
                                </tr>
                                <tr >
                                    <td class="td">
                                        Agidingbi Ikeja, Lagos Nigeria.
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class=""> <br>
                            <table>
                                <tr>
                                    <th style="font-family: montserratReg;">
                                        Phone Numbers:
                                    </th>
                                </tr>
                                <tr>
                                    <td class="td">
                                        <i class="fa fa-phone" style="padding-right: 10px; color: #77C53E;" aria-hidden="true"></i>
                                        +234 906 039 198
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
            include ("footer.php");
        ?>

        <script src="js/toucheffects.js"></script>
    </body>
</html>