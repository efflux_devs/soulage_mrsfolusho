<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Soulage | WHo We Are</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="content/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
		<link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
		<script src="content/js/jquery-1.11.0.min.js"></script>
		<script src="app/lib/angular.min.js"></script>
		<script src="app/lib/angular-route.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="app/routes.js"></script>
		<!--start-smoth-scrolling-->
		<script type="text/javascript" src="content/js/move-top.js"></script>
		<script type="text/javascript" src="content/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="content/js/modernizr.custom.97074.js"></script>
		<script src="content/js/jquery.chocolat.js"></script>
			<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
			<!--light-box-files -->
			<script type="text/javascript" charset="utf-8">
			$(function() {
				$('.gallery-grids a').Chocolat();
			});
		</script>
		<style type="text/css">
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.dropdown-menu {
				top: 95% !important;
			}
			.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
				background-color: red;
				width: 100%;
			}
			.top-menu ul li a {
				width: 100%;
			}
		</style>
	</head>
<body>
	<?php
        include ("header.php");
    ?>

	<div class="">
		<div class="who_are_we_col" style="background-color: #F2F2F2; padding: 30px;">
			<div class="container">
				<div class="col-md-12 w3layouts_register_right">
					<h1 style="font-size:36px; font-weight:600; color:#251021; text-transform: uppercase; text-align: center; font-family: montserratReg;">
						WHO WE ARE
					</h1>
					<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #FF000D; margin-bottom: 2em;"></p>

					<div class="container">
						<p style="color: #777777; padding: 20px 20px; font-size: 15px; line-height: 30px; text-align: justify;">
							Soulage Foundation is a non-governmental organization founded in Nigeria in 2011. Soulage has served indigent patients living with sickle cell disorder by supporting them with free medical services through an established Health Maintenance Organization. Late Mr. Olubiyi Odegbaike founded the organization to alleviate the ordeals of patients living with Sickle-cell disorder (SCD) and improve the quality of life of persons battling this genetic blood disorder.<br><br>


							The founder of soulage foundation Late Mr. Olubiyi Odegbaike himself had first hand experience of the disorder. He understood what people with the disorder go through and sought to use the foundation to reach out to indigent people living with the disorder. 
							Sickle-cell disease (SCD) is a genetic blood disorder that affects the haemoglobin within the red blood cells. The recurrent pain and complications caused by the disorder can interfere with many aspects of the patient's life, including education, employment and psychosocial development. Figures from the national Bureau of statistics show that an estimated 150,000 children are born every year in Nigeria with this trait. The SCD affects a large percentage of the Nigerian adult population. <br> 
						</p>

						<p style="font-size: 15px; padding: 20px 20px 20px 10px; background-color: #251021; margin-left: 20px; border-left: 10px solid #FF000D; color: #fff; text-align: justify;">
							<em>
								Majority of indigent children living with Sickle cell disorder have little or no access to healthcare, required diet and information on how to manage the disorder.
							</em>
						</p>

						<p style="font-size: 15px; padding: 20px 20px; line-height: 30px; color: #777777; font-weight: 600;">
							THE SOULAGE FOUNDATION AIMS TO ACHIEVE THE FOLLOWING OBJECTIVES;
						</p>

						<ul style="padding: 0px 30px;">
							<li style="line-height: 30px; color: #777777;">
								To provide free medical support to indigent patients of SCD.
							</li>

							<li style="line-height: 30px; color: #777777;">
								To alleviate the sufferings of patients living with sickle cell disorder by providing free routine drugs.
							</li>

							<li style="line-height: 30px; color: #777777;">
								To create awareness through lectures, seminars, workshops, conferences and campaigns about sickle cell disease at grassroots level and society at large.
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	
	<!--footer-->
		<?php 
			include ("footer.php");
		?>
	<!--//footer-->	

	<!-- banner Slider starts Here -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
		  // Slideshow 3
		  $("#slider3").responsiveSlides({
			auto:true,
			pager:false,
			nav: true,
			speed: 500,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
	</script>
	<script src="js/SmoothScroll.min.js"></script>
	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->  

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
	<script>
		var myIndex = 0;
		carousel();

		function carousel() {
		    var i;
		    var x = document.getElementsByClassName("mySlides");
		    for (i = 0; i < x.length; i++) {
		       x[i].style.display = "none";  
		    }
		    myIndex++;
		    if (myIndex > x.length) {myIndex = 1}    
		    x[myIndex-1].style.display = "block";  
		    setTimeout(carousel, 3000); // Change image every 2 seconds
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        var panel = this.nextElementSibling;
		        if (panel.style.display === "block") {
		            panel.style.display = "none";
		        } else {
		            panel.style.display = "block";
		        }
		    }
		}
	</script>

	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = panel.scrollHeight + "px";
		    } 
		  }
		}
	</script>
</body>
</html>