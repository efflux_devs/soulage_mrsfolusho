<head>
    <title>Portfolio | Soulage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
     <link rel="stylesheet" type="text/css" media="all" href="c/css/animate.css">
     <link rel="shortcut icon" type="image/png" href="favicon.png"/>
    <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="content/css/style.css" rel='stylesheet' type='text/css' />
    <link href="content/css/index.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
    <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
    <script src="content/js/jquery-1.11.0.min.js"></script>
    <script src="app/lib/angular.min.js"></script>
    <script src="app/lib/angular-route.min.js"></script>
    <link href="content/css/full-slider.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="app/routes.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script type="text/javascript" src="content/js/move-top.js"></script>
    <script type="text/javascript" src="content/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
    </script>

    <script>
          $( function() {
            $( "#tabs" ).tabs();
          } );
    </script>

    <style>
        .dropdown-menu a:hover {background-color: #f1f1f1;}
        .dropdown:hover .dropdown-menu {
            display: block;
        }
        .dropdown-menu a {
            text-transform: uppercase;
        }
        .img-responsive {
            height: 100% !important;
        }
    </style>



    <script>
          $( function() {
            $( "#tabs" ).tabs();
          } );
    </script>

    <style>
        .dropdown-menu a:hover {background-color: #f1f1f1;}
        .dropdown:hover .dropdown-menu {
            display: block;
        }
        .dropdown-menu a {
            text-transform: uppercase;
        }
        .img-responsive {
            height: 100% !important;
        }
    </style>
</head>
<body style="background: #fff;">

    <?php
        include ("header.php");
    ?>

    <div class="container project_css">
        <div class="row">
            <div class="col-md-12">
                <div class="main_body">
                    <table class="table_2">
                        <tr>
                            <th style="color: #251021;">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                Blood Drive
                            </th>
                        </tr>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <table>
                                    <tr>
                                        <td style="text-align: justify;">
                                            Blood Drive Summary: <br>
                                            ACHEIVEMENTS<br><br>
                                            Body mass indices were done to identify corps members who are eligible to donate blood.
                                            <br> <br>
                                            We were able to surprisingly get between 60-70 pint of blood for the blood bank.
                                            <br> <br>
                                            Free genotype tests and blood group tests were conducted for about 200 corps members.
                                            <br><br>
                                            We were able to educate the corps members on the importance of knowing their body mass before donating blood anywhere.
                                            <br><br>
                                            Corps members were further educated on Sickle cell crisis management.
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="gallery" style="padding: 0px !important;">
                                <div class="">
                                    <div class="wthree_gallery_grids">
                                        <div id="jzBox" class="jzBox">
                                            <div id="jzBoxNextBig"></div>
                                            <div id="jzBoxPrevBig"></div>
                                            <img src="#" id="jzBoxTargetImg" alt=" " />
                                            <div id="jzBoxBottom">
                                                <div id="jzBoxTitle"></div>
                                                <span id="jzBoxMoreItems">
                                                    <div id="jzBoxCounter"></div>
                                                    <i class="arrow-left" id="jzBoxPrev"></i> 
                                                    <i class="arrow-right" id="jzBoxNext"></i> 
                                                </span>
                                                <i class="close" id="jzBoxClose"></i>
                                            </div>
                                        </div>
                                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                                    <div class="tab_img">
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/1.JPG" class="jzBoxLink" title="BLOOD DRIVE" style="background-size: 100% auto !important;">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/1.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/2.JPG" class="jzBoxLink" title="BLOOD DRIVE">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/2.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>

                                                    <div class="tab_img">
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/3.JPG" class="jzBoxLink" title="BLOOD DRIVE">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/3.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/4.JPG" class="jzBoxLink" title="BLOOD DRIVE">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/4.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>

                                                    <div class="tab_img">
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/5.JPG" class="jzBoxLink" title="BLOOD DRIVE">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/5.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-6 agile_gallery_grids">
                                                            <a href="blooddrive/6.JPG" class="jzBoxLink" title="BLOOD DRIVE">
                                                                <div class="view view-sixth">
                                                                    <img src="blooddrive/6.JPG" alt=" " class="img-responsive" />
                                                                    <div class="mask">
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script src="content/js/jzBox.js"></script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    <?php
        include ("footer.php");
    ?> 
</body>