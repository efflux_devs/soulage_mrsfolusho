<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Donate | Soulage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="content/css/style.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
        <link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
        <script src="content/js/jquery-1.11.0.min.js"></script>
        <script src="app/lib/angular.min.js"></script>
        <script src="app/lib/angular-route.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="app/routes.js"></script>
        <!--start-smooth-scrolling-->
        <script type="text/javascript" src="content/js/move-top.js"></script>
        <script type="text/javascript" src="content/js/easing.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script src="content/js/modernizr.custom.97074.js"></script>
        <script src="content/js/jquery.chocolat.js"></script>
            <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
            <!--light-box-files -->
            <script type="text/javascript" charset="utf-8">
            $(function() {
                $('.gallery-grids a').Chocolat();
            });
        </script>
    </head>
<body>

	<?php
        include ("header.php");
    ?> 
	
	<section class="banner-w3ls2 donate_1">
		<div class="container donate_2">
			<h1 class="text-center agileits-w3layouts agile w3-agile" style="color: #B0CE2D; font-weight: 600; margin-top: 150px !important;">
				<!-- Giving The Best Solutions -->
			</h1>
		</div>
	</section>
<!-- //main -->
	<!-- Services --> 
		<div class="popular-w3" id="services">
			<div class="thecontent" style="background-color: #251021;-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px #888; box-shadow: 0 0 5px #888; height: auto; padding: 50px 40px;">

				<p style="text-transform: uppercase; color: #fff; font-family: montserratReg; font-size: 30px; text-align: center;">
					Take action by making a donation to Soulage Children
				</p><br>
				<h1 style="color: #fff; text-align: center; functionmontserratReg">Account Details</h1>
				<p style="color: #fff; text-align: center;">
					Account Name: Olubiyi Odegbaike Soulage Foundation For Sickle Cell <br>
					Account Number: 0238663263 (CURRENT ACCOUNT) <br>
					Bank: Gtbank
				</p>
			</div>
		</div>
	<!-- //Services --> 

	<!-- team -->
	<script src="js/jquery.vide.min.js"></script>
	
	<section class="banner-w3ls3">
		<div class="container">
		</div>
	</section>

	<?php
        include ("footer.php");
    ?> 


<!-- js-scripts -->					
		
			<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
			
		<!-- Baneer-js -->
		<script src="js/responsiveslides.min.js"></script>
		<script>
				$(function () {
					$("#slider").responsiveSlides({
						auto: true,
						pager:false,
						nav: true,
						speed: 1000,
						namespace: "callbacks",
						before: function () {
							$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
						}
					});
				});
			</script>
		<!-- //Baneer-js -->
		
		<!-- For-Gallery-js -->
			<!-- script for portfolio -->
			<script type='text/javascript' src='js/jquery.easy-gallery.js' ></script>
			<script type='text/javascript'>
			  //init Gallery
			  $('.portfolio').easyGallery();
			</script>
			<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
			<script type="text/javascript">
				$(document).ready(function () {
					$('#horizontalTab').easyResponsiveTabs({
						type: 'default', //Types: default, vertical, accordion           
						width: 'auto', //auto or any width like 600px
						fit: true   // 100% fit in a container
					});
				});		
			</script>
			<!-- //script for portfolio -->
		<!-- //For-Gallery-js -->

		<!-- start-smoth-scrolling -->
				<script type="text/javascript" src="js/move-top.js"></script>
				<script type="text/javascript" src="js/easing.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
				</script>
		<!-- start-smoth-scrolling -->

<!-- //js-scripts -->
</body>
</html>