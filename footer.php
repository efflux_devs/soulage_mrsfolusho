<!--footer-starts-->
	<div class="footer">
		<div class="container">
			<div class="footer-text">
				<div class="col-md-12">
					<p  style="color: #fff;">© 2016 Soulage. All Rights Reserved | Powered by  <a href="http://www.greymatteragency.com/" target="_blank">Greymatter Agency</a> </p>
				</div>
				<div class="clearfix"></div>				
			</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				*/
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
<!--footer-end-->