<!DOCTYPE html>
<html>
	<head>
		<title>Soulage | About</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="content/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
		<link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
		<script src="content/js/jquery-1.11.0.min.js"></script>
		<script src="app/lib/angular.min.js"></script>
		<script src="app/lib/angular-route.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="app/routes.js"></script>
		<!--start-smoth-scrolling-->
		<script type="text/javascript" src="content/js/move-top.js"></script>
		<script type="text/javascript" src="content/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="content/js/modernizr.custom.97074.js"></script>
		<script src="content/js/jquery.chocolat.js"></script>
			<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
			<!--light-box-files -->
			<script type="text/javascript" charset="utf-8">
			$(function() {
				$('.gallery-grids a').Chocolat();
			});
		</script>
	</head>
	<body>
		<?php
            include ("header.php");
        ?>

		<!--about-starts-->
			<div class="about">
				<div class="container">
					<div class="about-top heading">
						<h2 style="color: #FF0000;">About Us</h2>
					</div>
					<div class="about-bottom">
						<div class="col-md-6 about-left">
							<h4>
								Why Sickle Cell?
							</h4>
							<p>
								The founder of  soulage foundation Late Mr. Olubiyi Odegbaike himself had first hand experience of the disorder. He understood what  people with the disorder go through and sought to use the foundation to reach out to indigent people living with the disorder.
								<br/>
								Sickle-cell disease (SCD) is a genetic blood disorder that affects the haemoglobin within the red blood cells. The recurrent pain and complications caused by the disorder can interfere with many aspects of the patient's life, including education, employment and psychosocial development. Figures from the national Bureau of statistics show that an estimated 150,000 children are born every year in Nigeria with this trait. The SCD affects a large percentage of the Nigerian adult population.
							</p>
						</div>

						<div class="col-md-6 about-left">
							<img src="content/images/s3.jpg" alt="" />
						</div>

						<div class="clearfix"></div>
					</div>
					<p style="font-size: 15px; color: #000; text-align: center; margin-top: 20px; font-family: CaviarDreams;">
						Majority of  indigent children living with Sickle cell disorder have little or no access to healthcare, required diet and information on how to manage the disorder.
					</p>
				</div>
			</div>
		<!--about-end-->

		<!--why-starts-->
			<div class="why">
				<div class="container">
					<div class="why-top heading">
						<h3 style="color: #FF0000;">
							Our Aims & Objectives
						</h3>
					</div>
					<div class="why-bottom">
						<div class="col-md-6 why-left">
							<img src="content/images/s1.jpg" alt="" />
						</div>
						<div class="col-md-6 why-left">
							<h4 style="color: #0F3E50;">
								The Soulage foundation aims to achieve the following objectives;
							</h4>
							<div class="why-one">
								<div class="wh-left">
									<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
								</div>
								<div class="wh-right">
									<ul style="font-size: 15px; color: #999;">
										<li style="margin-bottom: 20px; font-family: CaviarDreams; color: #000;">
											To provide free medical support to indigent patients of SCD.
										</li>
										<li style="margin-bottom: 20px; font-family: CaviarDreams; color: #000;">
											To alleviate the sufferings of patients living with sickle cell disorder by providing free routine drugs.
										</li>
										<li style="margin-bottom: 0px; font-family: CaviarDreams; color: #000;">
											To create awareness through lectures, seminars, workshops, conferences and campaigns about sickle cell disease at grassroots level and society at large.
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		<!--why-end-->

		<!--team-starts-->
			<div class="team">
				<div class="container">
					<div class="team-top heading">
						<h3 style="color: #FF0000;">Our Team</h3>
					</div>
					<div class="team-bottom">
						<div class="col-md-3 team-left" data-toggle="modal" data-target="#myModal">
							<img src="content/images/biyi.jpg" alt="" style="height: 250px;" />
							<div class="team-text" >
								<h4>LATE BIYI ODEGBAIKE</h4>
							</div>
						</div>
						<div class="col-md-3 team-left" data-toggle="modal" data-target="#myModal_1">
							<img src="content/images/folusho_ode.jpg" alt="" style="height: 250px;" />
							<div class="team-text">
								<h4>FOLUSHO ODEGBAIKE</h4>
							</div>
						</div>
						<div class="col-md-3 team-left" data-toggle="modal" data-target="#myModal_2">
							<img src="content/images/ike_adebowale.jpg" alt="" style="height: 250px;" />
							<div class="team-text">
								<h4>IKEOLUWAPO ADEBOWALE</h4>
							</div>
						</div>
						<div class="col-md-3 team-left" data-toggle="modal" data-target="#myModal_3">
							<img src="content/images/shola_ayeni.jpg" alt="" style="height: 250px;" />
							<div class="team-text">
								<h4>OLUSHOLA AYENI</h4>
							</div>
						</div>
						<div class="col-md-3 team-left" data-toggle="modal" data-target="#myModal_4">
							<img src="content/images/seun_ayinla.jpg" alt="" style="height: 250px;" />
							<div class="team-text">
								<h4>OLUWASEUN O. AYINLA</h4>
							</div>
						</div>	
						<div class="clearfix"></div>
					</div>

					<div class="col-sm-12">
						<div class="row">
						 	<div class="col-sm-7 five-three">
							    <div class="row">
								    <div class="col-sm-4" style="cursor: pointer;" data-toggle="modal" data-target="#myModal">
								     	<img src="content/images/biyi.jpg" alt="" style="width: 100% !important; height: 250px;"/>
										<div class="team-text" >
											<h4>
												<a href="javascript:void(0)">LATE BIYI ODEGBAIKE</a>
											</h4>
										</div>
								    </div>

								    <div class="col-sm-4" style="cursor: pointer;" data-toggle="modal" data-target="#myModal_1">
								     	<img src="content/images/folusho_ode.jpg" alt="" style="width: 100% !important; height: 250px;"/>
										<div class="team-text">
											<h4>FOLUSHO ODEGBAIKE</h4>
										</div>
								    </div>

								    <div class="col-sm-4" style="cursor: pointer;" data-toggle="modal" data-target="#myModal_2">
								    	<img src="content/images/ike_adebowale.jpg" alt="" style="width: 100% !important; height: 250px;"/>
										<div class="team-text">
											<h4>IKEOLUWAPO ADEBOWALE</h4>
										</div>
								    </div><!-- end inner row -->
							    </div>
							</div>

						    <div class="col-sm-5 five-two">
							    <div class="row">
							    	<div class="col-sm-6" style="cursor: pointer;" data-toggle="modal" data-target="#myModal_3">
							        	<img src="content/images/shola_ayeni.jpg" alt="" style="width: 100% !important; height: 250px;"/>
										<div class="team-text">
											<h4>OLUSHOLA AYENI</h4>
										</div>
							    	</div>

							    	<div class="col-sm-6" style="cursor: pointer;" data-toggle="modal" data-target="#myModal_4">
							        	<img src="content/images/seun_ayinla.jpg" alt="" style="width: 100% !important; height: 250px;"/>
										<div class="team-text">
											<h4>OLUWASEUN O. AYINLA</h4>
										</div>
							    	</div>
							    </div><!-- end inner row -->
							</div>
						</div><!-- end outer row -->
					</div>

					<div class="container">
						<!-- Modal -->
						  	<div class="modal fade" id="myModal" role="dialog">
							    <div class="modal-dialog">
							    	<!-- Modal content-->
							      		<div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal">&times;</button>
									          <h4 class="modal-title">BIYI ODEGBAIKE</h4>
									        </div>
									        <div class="modal-body">
									          	<p style="font-family: CaviarDreams;">
									          		Biyi Odegbaike always had a quiet and calm mien; though he was the last born, he was very considerate 
									          		and thoughtful oftentimes looking out for the wellbeing of others, which is not expected of his 
									          		position in the family.
									          		<br><br>
									          		Biyi was very entrepreneurial; he setup Promoscence branding enterprise, the first company to introduce 
									          		advertising via sight and smell in Nigeria. In addition Biyi set-up a Security solutions company 
									          		Waregard, with a focus on providing a wide range of security equipment for companies in Nigeria.
									          		<br><br>
									          		A meaningful life for Biyi meant reaching out to the needy; he set-up Soulage Foundation with a mission 
									          		to provide basic drugs and financial age for the less privileged of the Society with Sick-cell anemia.
									          		<br><br>
									          		Biyi was an active member of Fountain of Life Church and an active member of Discovery for men.
									          		<br><br>
									          		Biyi attended University of Ilorin and there he met his heartthrob Foluso. 
									          		Biyi and Foluso got married in December 2005. They had their first child, a son, 
									          		Enioluwa in March 2010 and in September 2013, their daughter Monioluwa was born.
									          	</p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
								      	</div>
							    </div>
						  	</div>

						  	<div class="modal fade" id="myModal_1" role="dialog">
							    <div class="modal-dialog">
							    	<!-- Modal content-->
							      		<div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal">&times;</button>
									          <h4 class="modal-title">FOLUSHO ODEGBAIKE</h4>
									        </div>
									        <div class="modal-body">
									          <p style="font-family: CaviarDreams;">
									          		Folusho Odegbaike, the lead consultant at HYT Consulting is an experienced Organizational Development 
									          		Professional with expertise in People Development, Process Re-engineering and Resources Optimization. 
									          		She is a certified SAP HMC consultant and an accomplished human capital management consultant with over 
									          		15 years work experience in Human Capital Learning and Development, Performance Management, Customer 
									          		Service and other core Human Resources and Administration functions, which has given her a clear edge as a 
									          		Human Capital Management Professional. 
									          		<br><br>
									          		As the head of Human Resources in PNN, Folusho was in charge of the entire HR operations, and 
									          		successfully restructured the HR division in line with best practices, which ultimately saw the 
									          		company with over 700 staff meet its corporate objectives by adapting Human Resources strategies 
									          		in conjunction with stakeholder to deliver commercial success.
									          		<br><br>
									          		In 2008, Folusho resigned from Zain Nigeria (Now Airtel) after seven years of working in various 
									          		departments within the organization. Before her resignation, she was a Learning/Training and 
									          		Development Specialist where she was responsible for the development of needs assessment and 
									          		training of staff to fill the competency gaps identified during performance management process 
									          		and development needs analysis. 
									          		<br><br>
									          		Folusho obtained her Master’s degree in Business Administration from University of Lagos. 
									          		She is also a certified SAP HCM and project management professional with a sound knowledge of Prince2 Project Management 
									          		Methodology. She is a member of a number of professional institutes including the Society of Human Resources Management (SHRM), 
									          		Chartered Institute of Personnel Development U.K (CIPD), Nigerian Institute of Training & Development (NITAD).
									          </p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
								      	</div>
							    </div>
						  	</div>

						  	<div class="modal fade" id="myModal_2" role="dialog">
							    <div class="modal-dialog">
							    	<!-- Modal content-->
							      		<div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal">&times;</button>
									          <h4 class="modal-title">IKEOLUWAPO ADEBOWALE</h4>
									        </div>
									        <div class="modal-body">
									          <p style="font-family: CaviarDreams;">
									          		Ikeoluwapo Adebowale is an HR Professional with over 15 years work experience of which 11 years is in 
									          		Human Resources practice with exposure to Best Practice in various industries. 
									          		<br><br>
									          		Ikeoluwapo has functioned within the backend areas of Human Resources and these areas include HR 
									          		Information Systems, performance management, Compensation & Benefits, Personnel Administration, 
									          		Resourcing and Payroll where her achievements has spanned redesigning processes, development and 
									          		implementation of policies and organizational restructuring. 
									          		<br><br>
									          		She has also functioned within the frontend as a Human Resource Business Partner where interaction 
									          		with and coaching of leadership, implementation of company strategies, change management and 
									          		organizational development is key.
									          		<br><br>
									          		As the VP of Human Capital Management in a leading e-commerce company Ikeoluwapo possesses substantial 
									          		understanding of the related existing Nigerian policies, regulations and guidelines in addition to 
									          		excellent inter-personal skills, follow process and make improvements. The soft skills she has developed 
									          		over the years of being an HR Professional is also complemented with her being a Chartered Accountant 
									          		hence enhancing the ability to weigh the impact of HR decisions on the bottom line. She has an MBA from 
									          		Manchester Business School as well as holds the ACIPM and GPHR qualification.
									          </p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
								      	</div>
							    </div>
						  	</div>

						  	<div class="modal fade" id="myModal_3" role="dialog">
							    <div class="modal-dialog">
							    	<!-- Modal content-->
							      		<div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal">&times;</button>
									          <h4 class="modal-title">OLUSHOLA AYENI</h4>
									        </div>
									        <div class="modal-body">
									        	<p style="font-family: CaviarDreams;">
									          		She is the Chief Operating Officer in Cloud interactive Limited and coordinates the operations 
									          		in the West African region in the company. She holds a B.Sc Honors Degree in Education and Economics 
									          		and an Associate Membership at the Nigerian Institute of Brand Management.  <br><br>
									          		Olushola acquired work experience over the years by working for several Blue Chip Companies 
									          		across Africa, Europe and the Middle East occupying key positions and overseeing several critical 
									          		business projects. Her experience, spanning over thirteen years cuts across the Creative, Hospitality, 
									          		Banking and Telecommunication Industries. <br><br>
									          		She is also a consultant, entrepreneur specializing in investment and sales. 
									          		Her last shot before moving to Cloud Inetractive Limited was at Zain Network (Now Airtel) International, 
									          		the telecommunication giant situated in Manama, the Kingdom of Bahrain.
							          			</p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
								      	</div>
							    </div>
						  	</div>

						  	<div class="modal fade" id="myModal_4" role="dialog">
							    <div class="modal-dialog">
							    	<!-- Modal content-->
							      		<div class="modal-content">
									        <div class="modal-header">
									          <button type="button" class="close" data-dismiss="modal">&times;</button>
									          <h4 class="modal-title">OLUWASEUN O. AYINLA</h4>
									        </div>
									        <div class="modal-body">
									          <p style="font-family: CaviarDreams;">
									          		Seun Ayinla is a professional with over 13 years work experience in strategy, brand management, 
									          		mergers and acquisition. She is an operation expert and internal audit with track record of delivering 
									          		results focused on business acquisition, quality assurance, productivity, and consumer driven 
									          		bottom-line growth.  <br><br>
									          		As a senior Brand Manager in Dangote Flour Mills (Formerly Tiger Brands), she delivers sustainable growth 
									          		and improve market share by delivering the brand promise while growing brand awareness through effective 
									          		business strategies based on regional consumer insight. She also served in the company as the group business 
									          		nalyst; Manager new categories and strategic initiatives. <br><br>
									          		In 2011 Seun Resigned from Aecom Technology Corporation – Los Angeles, 
									          		CA as the Internal audit Senior where she had the responsibility of executing risk assessment programmes, 
									          		identifying strategic operational, reporting and compliance risk while overseeing the process of internal 
									          		control over financial reporting.<br><br>
									          		She Resigned from Deloitte& Touche New York NY as a senior consultant, enterprise risk services in 2007.
									          		hile in Deloitte & Touche she led and performed internal audit reviews and projects for various 
									          		multinational companies (i.e financial, operational and compliance).<br><br>
									          		She obtained her bachelor’s degree in Accounting internal audit and finance from state university of 
									          		New York at Buffalo.  She is also a certified internal audit professional  - CIA. 
									          </p>
									        </div>
									        <div class="modal-footer">
									          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									        </div>
								      	</div>
							    </div>
						  	</div>
					</div>
				</div>
			</div>
		<!--team-end-->

		<?php
            include ("footer.php");
        ?>
	</body>
</html>