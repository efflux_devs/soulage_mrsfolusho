<!doctype html>
<html>
	<head>
		<title>Soulage | About Us</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="Charity Website, N.G.O Website, Sickle Cell Website,"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href="content/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="content/css/style.css" rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="content/css/fonts/font.css">
		<link rel="stylesheet" href="content/css/font-awesome/css/font-awesome.min.css">
		<script src="content/js/jquery-1.11.0.min.js"></script>
		<script src="app/lib/angular.min.js"></script>
		<script src="app/lib/angular-route.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="app/routes.js"></script>
		<!--start-smooth-scrolling-->
		<script type="text/javascript" src="content/js/move-top.js"></script>
		<script type="text/javascript" src="content/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<script src="content/js/modernizr.custom.97074.js"></script>
		<script src="content/js/jquery.chocolat.js"></script>
			<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
			<!--light-box-files -->
			<script type="text/javascript" charset="utf-8">
			$(function() {
				$('.gallery-grids a').Chocolat();
			});
		</script>
		<style type="text/css">
			.dropdown-menu a:hover {background-color: #f1f1f1;}
			.dropdown:hover .dropdown-menu {
			    display: block;
			}
			.dropdown-menu a {
				text-transform: uppercase;
			}
			.dropdown-menu {
				top: 95% !important;
			}
			.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
				background-color: red;
				width: 100%;
			}
			.top-menu ul li a {
				width: 100%;
			}
		</style>
	</head>
	<body>
		<?php
	        include ("header.php");
	    ?>

		<section class="info-w3ls2">
			<div class="container">
				<h3 class="text-center agileits-w3layouts agile w3-agile" style="padding: 0px;">Our Vision and Mission</h3>
				<p style="text-align: center; width: 3%; margin: auto; height: 7px; background-color: #FF000D; margin-bottom: 2em;"></p>
				<p class="text-center" style="text-align: justify; color: #777777; font-size: 15px; font-family: opensans;">
					Our <b>VISION</b> is to support 5000 indigent patients living with Sickle Cell disorder and alleviate the sufferings of people with sickle cell disorder in every way possible.

					<br> <br> Our <b>MISSION</b> is to reduce the pains of indigent living with sickle cell disorder by providing free medical support; provide enlightenment and awareness and to help improve the quality of life of indigent patients living with SCD.
				</p>
			</div>
		</section>
		<section class="banner-w33ls3">
			<div class="container">
				<h3 class="text-center" style="color: #fff !important;">Hold the vision, Trust the process.</h3>
			</div>
		</section>
		<?php
	        include ("footer.php");
	    ?>
	</body>
</html>